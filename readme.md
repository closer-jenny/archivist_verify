## [Verify questionnaire metadata](https://wiki.ucl.ac.uk/display/CTTEAM/Metadata+Pipeline)


### input.txt
- The archivist instance, for example: closer-archivist-alspac
- The prefix, for example: alspac\_04\_tf1pi.
- Note that this field can be blank, i.e. export PREFIX=


### output in csv format
|    |  Check                                                                               | Output Table                              |
| -: | :----------------------------------------------------------------------------------- | :---------------------------------------- |
|  1 | questionItem labels start with qi_                                                   | 01\_qi\_label                             |
|  2 | questionConstruct labels start with qc_                                              | 02\_qc\_label                             |
|  3 | codeList labels start with cs_                                                       | 03\_cs\_label                             |
|  4 | questionItems and questionConstruct labels match                                     | 04\_qi\_qc\_label                         |
|  5 | questions have a response                                                            | 05\_qi\_response                          |
|  6 | question has a literal                                                               | 06\_qi\_literal                           |
|  7 | Agency matches the CV                                                                | 07\_agency                                |
|  8 | Study matches the agency                                                             | 08\_study\_agency                         |
|  9 | labels do not contain space                                                          | 09\_labels\_w\_space                      |
| 10 | no missing roman numerals on labels                                                  | 10\_missing\_roman\_numeral\_labels       |
| 11 | condition labels match the first question in the logic                               | 11\_condition\_label\_logic               |
| 12 | condition logic is blank, label after the first question inside the condition        | 12\_condition\_label\_first\_question     |
| 13 | condition logic contains only loop, labels match first question inside the condition | 13\_condition\_label\_loop\_no\_question  |
| 14 | output condition logic contains both loop and question                               | 14\_condition\_label\_loop\_and\_question |
| 15 | sequence is not empty                                                                | 15\_empty\_sequence                       |
| 16 | loop is not empty                                                                    | 16\_empty\_loop                           |
| 17 | condition is not empty                                                               | 17\_empty\_condition                      |

- The .csv files in the main output directory lists failed checks for all instruments in the instance.
- There is a separate folder named prefix_dir which contains the output .txt for the prefix in the input.txt.

- output.txt is output file from "verify_questionnaire_metadata.sql", it is a summary of the number of potential issues it has found.

### Note
- There are cases such as the missing roman numerals on labels is actually from a label which contains a space.
In this case, after remove the space from the label, the same label will have all roman numerals.


