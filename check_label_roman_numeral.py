#!/usr/bin/env python3

"""
Find all labels ending with roman numeral, then check if there are any missing numbers.
"""

import pandas as pd


def checkIfRomanNumeral(numeral):
    """
    check the input contains valid roman numerals
    """
    numeral = numeral.upper()
    validRomanNumerals = ["M", "D", "C", "L", "X", "V", "I", "(", ")"]
    valid = True
    for letters in numeral:
        if letters not in validRomanNumerals:
            # print("Sorry that is not a valid roman numeral")
            valid = False
            break
    return valid


# https://bas.codes/posts/python-roman-numerals
def to_roman_numeral(value):
    """
    convert a number into roman numeral
    """
    roman_map = {
        1: "I",
        5: "V",
        10: "X",
        50: "L",
        100: "C",
        500: "D",
        1000: "M",
    }
    result = ""
    remainder = value

    for i in sorted(roman_map.keys(), reverse=True):
        if remainder > 0:
            multiplier = i
            roman_digit = roman_map[i]

            times = remainder // multiplier
            remainder = remainder % multiplier
            result += roman_digit * times

    return result


def from_roman_numeral(numeral):
    """
    convert a roman numeral into an integer
    """
    value_map = {"I": 1,
                 "V": 5,
                 "X": 10,
                 "L": 50,
                 "C": 100,
                 "D": 500,
                 "M": 1000}
    value = 0
    last_digit_value = 0

    for roman_digit in numeral[::-1]:
        digit_value = value_map[roman_digit]

        if digit_value >= last_digit_value:
            value += digit_value
            last_digit_value = digit_value
        else:
            value -= digit_value

    return value


def find_missing_integer(integer_lst):
    """
    from a list of integers, find any missing numbers
    """
    # Create a frequency dictionary with keys ranging from the minimum to maximum value in the list
    freq_dict = {i:0 for i in range(min(integer_lst), max(integer_lst)+1)}

    # Iterate through the list and increment the frequency count for each value encountered
    for i in integer_lst:
        freq_dict[i] += 1

    # Return a list of all keys with frequency 0 (i.e., the missing values)
    return [key for key, val in freq_dict.items() if val == 0]


def find_missing_numeral(numeral_lst):
    """
    from a list of roman numerals, find any missing ones
    """
    # covert a roman numeral list to a integer list
    integer_lst = [from_roman_numeral(e.upper()) for e in numeral_lst]
    # find missing elements
    missing_integer_lst = find_missing_integer(integer_lst)
    # return missing list with roman numeral
    return [to_roman_numeral(e) for e in missing_integer_lst]


def main():

    # read in all_labels
    input_file = 'all_labels.csv'
    df_input = pd.read_csv(input_file)

    # output label which contains spaces
    df_label_space = df_input.loc[df_input['label'].str.contains(' ') == True]
    df_label_space.to_csv('09_labels_w_space.csv', index=False)

    # remove end white spaces from label
    df_input['label'] = df_input['label'].str.rstrip()

    # remove labels contains spaces
    # includes labels end with a space here
    df_input = df_input.loc[df_input['label'].str.contains(' ') == False]

    df_input['label_split_end'] = df_input['label'].apply(lambda x: x.split('_')[-1])
    df_input['label_split'] = df_input.apply(lambda row: row['label'].replace('_'+ row['label_split_end'], ''), axis=1)

    # subset to repeated first part of labels
    df_sub = df_input.loc[~df_input['label_split'].isin(['qi', 'qg', 'qc', 'l', 'c'])]

    df = df_input.copy()
    df['count'] = df.groupby(['prefix', 'label_split']).transform('size')
    df['sequence'] = df.groupby(['prefix', 'label_split']).cumcount() + 1
    df['next_value'] = df.groupby(['prefix', 'label_split'])['label_split_end'].shift(-1)

    # remove those appeared only once
    df_mul = df.loc[df['count'] > 1]

    # find labels with first ends starts from "i"
    df_select = df_mul.loc[(df_mul['label_split_end'] == 'i') & (df_mul['sequence'] == 1), ['prefix', 'label_split', 'next_value']]
    # remove i,j ... sequence
    df_rm_j = df_select.loc[df_select['next_value'] != 'j']

    # keep prefix/labels from df_select
    df_keep = df_mul.merge(df_rm_j, on = ['prefix', 'label_split'])

    # remove not ends with roman numbers
    df_keep['keep'] = df_keep['label_split_end'].apply(lambda x: checkIfRomanNumeral(x))
    df_keep = df_keep.loc[df_keep['keep'] == True]

    # interested in label base and list of roman numbers
    df_final = df_keep[['prefix', 'label_split', 'label_split_end']].groupby(['prefix', 'label_split']).agg(list).reset_index()

    # check if there are missing numbers
    df_final['missing_value'] = df_final['label_split_end'].apply(lambda x: find_missing_numeral(x))

    # Empty lists evaluate to False in a boolean context
    df_out = df_final.loc[df_final['missing_value'].astype(bool)]

    if df_out.empty:
        with open('no_missing_labels.csv', 'w') as the_file:
            the_file.write('No roman numerals missing in labels\n')
    else:
        df_out.to_csv('10_missing_roman_numeral_labels.csv', index=False)


if __name__ == "__main__":
    main()

