-- questionItem labels start with qi_ ;
\qecho '1. qi_label: questionItem labels NOT start with qi_';
COPY (
select i.prefix,
       q.id as question_item_id,
       q.label as question_item_label,
       q.literal as question_item_literal,
       q.instruction_id,
       q.instrument_id,
       q.question_type
from question_items q
left join instruments i on i.id = q.instrument_id
where lower(q.label) not like 'qi_%'
)
TO STDOUT WITH CSV HEADER \g 01_qi_label.csv


-- questionConstruct labels start with qc_ ;
\qecho '2. qc_label: questionConstruct labels NOT start with qc_ ';
COPY (
select i.prefix,
       q.id as cc_questions_id,
       q.instrument_id,
       q.question_id,
       q.question_type,
       q.response_unit_id,
       q.label as cc_questions_label,
       q.parent_id,
       q.parent_type,
       q.position,
       q.branch
from cc_questions q
left join instruments i on i.id = q.instrument_id
where lower(q.label) not like 'qc_%'
)
TO STDOUT WITH CSV HEADER \g 02_qc_label.csv



-- codeList labels start with cs_ ;
\qecho '3. cs_label: codeList labels NOT start with cs_';
COPY (
select i.prefix,
       c.id as code_lists_id,
       c.label as code_lists_label,
       c.instrument_id
from code_lists c
left join instruments i on i.id = c.instrument_id
where lower(c.label) not like 'cs_%'
)
TO STDOUT WITH CSV HEADER \g 03_cs_label.csv



-- questionItems and questionConstruct labels match;
\qecho '4. qi_qc_label: questionItems and questionConstruct labels Not match';
COPY (
select i.prefix,
       question_items.id as question_items_id,
       question_items.label as question_items_label,
       cc_questions.label as cc_questions_label,
       CASE
       when SUBSTRING(question_items.label, 4, length(question_items.label)) != SUBSTRING(cc_questions.label, 4, length(cc_questions.label)) then 0
       else 1
       end as check_label_same
from question_items
left join instruments i on i.id = question_items.instrument_id
left join cc_questions on cc_questions.question_id = question_items.id
where SUBSTRING(question_items.label, 4, length(question_items.label)) != SUBSTRING(cc_questions.label, 4, length(cc_questions.label))
)
TO STDOUT WITH CSV HEADER \g 04_qi_qc_label.csv


-- questions have a response;
\qecho '5. qi_response: question items do NOT have a response';
COPY (
select i.prefix,
       question_items.id as question_items_id,
       question_items.label as question_items_label,
       question_items.instrument_id,
       rds_qs.response_domain_id
from question_items
left join instruments i on i.id = question_items.instrument_id
left join rds_qs on question_items.id = rds_qs.question_id
where rds_qs.response_domain_id is null
)
TO STDOUT WITH CSV HEADER \g 05_qi_response.csv


-- question has a literal;
\qecho '6. qi_literal: question items do NOT have a literal';
COPY (
select i.prefix,
       question_items.id as question_items_id,
       question_items.label as question_items_label,
       question_items.literal as question_items_label,
       question_items.instruction_id,
       question_items.instrument_id,
       question_items.question_type
from question_items
left join instruments i on i.id = question_items.instrument_id
where question_items.literal is null
)
TO STDOUT WITH CSV HEADER \g 06_qi_literal.csv


-- Agency matches the CV;
\qecho '7. agency: Agencies do NOT matches the CV';
COPY (
select agency,
       prefix,
       label,
       study
from instruments
where agency not in ('uk.alspac',
                     'uk.cls.bcs70',
                     'uk.mrcleu-uos.hcs',
                     'uk.cls.mcs',
                     'uk.cls.ncds',
                     'uk.cls.nextsteps',
                     'uk.lha',
                     'uk.mrcleu-uos.sws',
                     'uk.iser.ukhls',
                     'uk.wchads',
                     'uk.mrcleu-uos.heaf',
                     'uk.ons',
                     'uk.genscot'
                     )
)
TO STDOUT WITH CSV HEADER \g 07_agency.csv


-- Study matches the agency;
\qecho '8. study_agency: Studies do NOT matches the agency';
COPY (
SELECT sub.*
FROM (SELECT agency,
             prefix,
             label,
             study,
             CASE
             WHEN upper(study) = 'ALSPAC' then 'ALSPAC'
             WHEN upper(study) = 'BCS' then 'BCS'
             WHEN upper(study) = 'HCS' then 'HCS'
             WHEN upper(study) = 'MCS' then 'MCS'
             WHEN upper(study) = 'NCDS' then 'NCDS'
             WHEN upper(study) = 'NEXT STEPS' then 'Next Steps'
             WHEN upper(study) = 'NSHD' then 'NSHD'
             WHEN upper(study) = 'SWS' then 'SWS'
             WHEN upper(study) = 'US' then 'US'
             WHEN upper(study) = 'WCHADS' then 'WCHADS'
             WHEN upper(study) = 'HEAF' then 'HEAF'
             WHEN upper(study) = 'ONLS' then 'ONLS'
             WHEN upper(study) = 'GENERATION SCOTLAND' then 'Generation Scotland'
             END as correct_study,
             CASE
             WHEN upper(study) = 'ALSPAC' then 'uk.alspac'
             WHEN upper(study) = 'BCS' then 'uk.cls.bcs70'
             WHEN upper(study) = 'HCS' then 'uk.mrcleu-uos.hcs'
             WHEN upper(study) = 'MCS' then 'uk.cls.mcs'
             WHEN upper(study) = 'NCDS' then 'uk.cls.ncds'
             WHEN upper(study) = 'NEXT STEPS' then 'uk.cls.nextsteps'
             WHEN upper(study) = 'NSHD' then 'uk.lha'
             WHEN upper(study) = 'SWS' then 'uk.mrcleu-uos.sws'
             WHEN upper(study) = 'US' then 'uk.iser.ukhls'
             WHEN upper(study) = 'WCHADS' then 'uk.wchads'
             WHEN upper(study) = 'HEAF' then 'uk.mrcleu-uos.heaf'
             WHEN upper(study) = 'ONLS' then 'uk.ons'
             WHEN upper(study) = 'GENERATION SCOTLAND' then 'uk.genscot'
             END as correct_agency
      FROM instruments
      ) sub
WHERE sub.study != sub.correct_study
OR    sub.agency != sub.correct_agency
)
TO STDOUT WITH CSV HEADER \g 08_study_agency.csv


-- output all labels from questions/loops/conditions ;
\qecho '9. all_labels: output all labels from questions/loops/conditions';
COPY (
select i.prefix as prefix,
       qi.label as label
from question_items qi
left join instruments i on i.id = qi.instrument_id
UNION
select i.prefix as prefix,
       qg.label as label
from question_grids qg
left join instruments i on i.id = qg.instrument_id
UNION
select i.prefix as prefix,
       qc.label as label
from cc_questions qc
left join instruments i on i.id = qc.instrument_id
UNION
select i.prefix as prefix,
       l.label as label
from cc_loops l
left join instruments i on i.id = l.instrument_id
UNION
select i.prefix as prefix,
       c.label as label
from cc_conditions c
left join instruments i on i.id = c.instrument_id
order by prefix, label
)
TO STDOUT WITH CSV HEADER \g all_labels.csv


-- condition labels match the first question in the logic ;
-- NOTE: only checked "==" case, TODO ==, >=, <=, <, >, and != can all be used;
\qecho '10. condition_label_logic: condition labels do NOT match the first question in the logic.';
COPY (
select i.prefix,
       c.label,
       c.logic,
       replace(split_part(substr(c.label, 4), '_', 1), ' ', '') as condition_label_name,
       replace(substr(split_part(split_part(c.logic, '||', 1), '==', 1), 4), ' ', '') as logic_question_name
from cc_conditions c
left join instruments i on i.id = c.instrument_id
where replace(split_part(substr(c.label, 4), '_', 1), ' ', '') != replace(substr(split_part(split_part(c.logic, '||', 1), '==', 1), 4), ' ', '')
)
TO STDOUT WITH CSV HEADER \g 11_condition_label_logic.csv


-- When the condition logic is blank, the condition is labeled after the first question inside the condition. ;
\qecho '11. condition_label_first_question: condition labels do NOT match the first question inside the condition';
COPY (
select DISTINCT ON (q.parent_id)
       i.prefix as prefix,
       q.label as question_label,
       c.label as condition_label
from cc_questions q
left join instruments i on i.id = q.instrument_id
left join cc_conditions c on q.parent_id = c.id
where q.parent_type = 'CcCondition'
and c.logic is null
and replace(q.label, 'qc_', '') != replace(c.label, 'c_', '')
order by q.parent_id, q.id
)
TO STDOUT WITH CSV HEADER \g 12_condition_label_first_question.csv


-- When the first item in the condition logic is a reference to a loop rather than a question.;
-- case 1: When a loop is the only thing referenced in the condition logic, the condition is labeled after the first question inside the condition ;
\qecho '12. condition_label_loop_no_question: all condition label and logic for condition logic refer to only loop, condition labels do NOT match the first question inside the condition';
COPY (
select DISTINCT ON (q.parent_id)
       i.prefix as prefix,
       q.label as question_label,
       c.label as condition_label
from cc_questions q
left join instruments i on i.id = q.instrument_id
left join cc_conditions c on q.parent_id = c.id
where q.parent_type = 'CcCondition'
and substring(c.logic, 1, 1) = '_' 
and (not c.logic like  '%||%' or not c.logic like  '%&&%')
and replace(q.label, 'qc_', '') != replace(c.label, 'c_q', '')
order by q.parent_id, q.id
)
TO STDOUT WITH CSV HEADER \g 13_condition_label_loop_no_question.csv

-- case 2: When a loop is referenced alongside a question in the condition logic, the condition is labeled after the first question in the condition logic
\qecho '13. condition_label_loop_and_question: all condition label and logic for condition logic refer to both loop and question';
COPY (
select i.prefix,
       c.label, 
       c.logic
from cc_conditions c
left join instruments i on i.id = c.instrument_id
where substring(c.logic, 1, 1) = '_' 
and (c.logic like  '%||%' or c.logic like  '%&&%')
)
TO STDOUT WITH CSV HEADER \g 14_condition_label_loop_and_question.csv


-- check empty constructs: sequence, loop and condition without any children;
\qecho '14. empty_sequence: sequence do NOT have any children';
COPY (
select t1.prefix, t1.sequence_label
from (
  select i.prefix as prefix,
       s.label as sequence_label
from cc_sequences s
left join instruments i on i.id = s.instrument_id
) t1
left join (
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_questions q
left join instruments i on i.id = q.instrument_id
left join cc_sequences s on s.id = q.parent_id
where q.parent_type = 'CcSequence'
UNION
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_conditions c
left join instruments i on i.id = c.instrument_id
left join cc_sequences s on s.id = c.parent_id
where c.parent_type = 'CcSequence'
UNION
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_loops l
left join instruments i on i.id = l.instrument_id
left join cc_sequences s on s.id = l.parent_id
where l.parent_type = 'CcSequence'
UNION
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_statements st
left join instruments i on i.id = st.instrument_id
left join cc_sequences s on s.id = st.parent_id
where st.parent_type = 'CcSequence'
UNION
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_sequences se
left join instruments i on i.id = se.instrument_id
left join cc_sequences s on s.id = se.parent_id
where se.parent_type = 'CcSequence'
) t2 
on t1.prefix = t2.prefix 
and t1.sequence_label = t2.parent_label
where t2.parent_label is null
and t1.sequence_label is not null
order by t1.prefix
)
TO STDOUT WITH CSV HEADER \g 15_empty_sequence.csv


\qecho '15. empty_loop: sequence do NOT have any children';
COPY (
select t1.prefix, t1.loop_label
from (
  select i.prefix as prefix,
       s.label as loop_label
from cc_loops s
left join instruments i on i.id = s.instrument_id
) t1
left join (
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_questions q
left join instruments i on i.id = q.instrument_id
left join cc_loops s on s.id = q.parent_id
where q.parent_type = 'CcLoop'
UNION
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_loops c
left join instruments i on i.id = c.instrument_id
left join cc_loops s on s.id = c.parent_id
where c.parent_type = 'CcLoop'
UNION
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_loops l
left join instruments i on i.id = l.instrument_id
left join cc_loops s on s.id = l.parent_id
where l.parent_type = 'CcLoop'
UNION
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_statements st
left join instruments i on i.id = st.instrument_id
left join cc_loops s on s.id = st.parent_id
where st.parent_type = 'CcLoop'
UNION
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_sequences se 
left join instruments i on i.id = se.instrument_id
left join cc_loops s on s.id = se.parent_id
where se.parent_type = 'CcLoop'
) t2 
on t1.prefix = t2.prefix 
and t1.loop_label = t2.parent_label
where t2.parent_label is null
and t1.loop_label is not null
order by t1.prefix
)
TO STDOUT WITH CSV HEADER \g 16_empty_loop.csv


\qecho '16. empty_condition: conditions do NOT have any children';
COPY (
select t1.prefix, t1.condition_label
from (
  select i.prefix as prefix,
       s.label as condition_label
from cc_conditions s
left join instruments i on i.id = s.instrument_id
) t1
left join (
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_questions q
left join instruments i on i.id = q.instrument_id
left join cc_conditions s on s.id = q.parent_id
where q.parent_type = 'CcCondition'
UNION
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_conditions c
left join instruments i on i.id = c.instrument_id
left join cc_conditions s on s.id = c.parent_id
where c.parent_type = 'CcCondition'
UNION
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_loops l
left join instruments i on i.id = l.instrument_id
left join cc_conditions s on s.id = l.parent_id
where l.parent_type = 'CcCondition'
UNION
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_statements st
left join instruments i on i.id = st.instrument_id
left join cc_conditions s on s.id = st.parent_id
where st.parent_type = 'CcCondition'
UNION
select distinct i.prefix as prefix,
       s.label as parent_label
from cc_sequences se 
left join instruments i on i.id = se.instrument_id
left join cc_conditions s on s.id = se.parent_id
where se.parent_type = 'CcCondition'
) t2 
on t1.prefix = t2.prefix 
and t1.condition_label = t2.parent_label
where t2.parent_label is null
and t1.condition_label is not null
order by t1.prefix
)
TO STDOUT WITH CSV HEADER \g 17_empty_condition.csv


